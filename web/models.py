from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import UserManager as DjangoUserManager, AbstractUser, PermissionsMixin
from django.db import models

# Create your models here.


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class UserManager(DjangoUserManager):
    def create_user(self, username, password=None, **extra_fields):
        user = self.model(email=username)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password=None, **extra_fields):
        user = self.model(email=username, is_staff=True, is_superuser=True)
        user.set_password(password)
        user.save()
        return user


class User(BaseModel, AbstractBaseUser, PermissionsMixin):
    objects = UserManager()
    USERNAME_FIELD = 'email'

    email = models.EmailField(unique=True)
    password = models.CharField(max_length=128)
    name = models.CharField(default='Анонимный', max_length=128)
    surname = models.CharField(default='Анонимус', max_length=128)
    birthdate = models.DateField(default='2021-12-25', max_length=128)
    avatar = models.ImageField(null=True, blank=True, upload_to='user_avatars/')
    is_staff = models.BooleanField(default=False)


class Post(BaseModel):
    text = models.CharField(null=True, blank=True, default=None, max_length=250)
    media1 = models.ImageField(null=True, blank=True, upload_to='post_pictures/')
    likes = models.IntegerField(default=0)
    comments = models.IntegerField(default=0)
    reposts = models.IntegerField(default=0)
    pinned = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
