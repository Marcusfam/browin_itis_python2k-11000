from web.models import User


def register_user(email: str, password: str, name: str, surname: str, birthdate: str, avatar) -> User:
    user = User(email=email, name=name, surname=surname, birthdate=birthdate, avatar=avatar)
    user.set_password(password)
    user.save()
    return user
