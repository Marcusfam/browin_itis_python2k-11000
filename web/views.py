from datetime import datetime

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import DatabaseError
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import UpdateView, FormView, CreateView, DetailView, ListView

from web.forms import RegistrationForm, LoginForm, PostForm
from web.models import Post
from web.services import register_user


def main_view(request):
	context = {
		'current_time': datetime.now()
	}
	return render(request, 'web/main.html', context)


def register_view(request):
	context = {'form': RegistrationForm()}
	if request.method == 'POST':
		form = RegistrationForm(request.POST, request.FILES)
		context['form'] = form
		if form.is_valid():
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			name = form.cleaned_data['name']
			surname = form.cleaned_data['surname']
			birthdate = form.cleaned_data['birthdate']
			avatar = form.cleaned_data['avatar']
			try:
				register_user(email, password, name, surname, birthdate, avatar)
				user = authenticate(request, email=email, password=password)
				login(request, user)
				return redirect('main')
			except DatabaseError:
				form.add_error(None, "Пользователь с данным адресом уже зарегестрирован")
	return render(request, 'web/register.html', context)


def login_view(request):
	form = LoginForm()
	context = {'form': form}
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			user = authenticate(request, email=email, password=password)
			if user is None:
				context['error'] = 'Email или пароль неверные'
				# TODO рассказать про non_field_errors
				# form.add_error(None, 'Email или пароль неверные')
			else:
				login(request, user)
				return redirect('main')
	return render(request, 'web/login.html', context)


def logout_view(request):
	logout(request)
	return redirect('main')


@login_required
def post_add_view(request):
	context = {'form': PostForm()}
	if request.method == 'POST':
		form = PostForm(request.POST, request.FILES)
		if form.is_valid():
			data = form.cleaned_data
			print(data)
			data['user_id'] = request.user.id
			Post.objects.create(**data)
			return redirect('main')
		context['form'] = form
	return render(request, 'web/post/add.html', context)


@login_required
def post_detail_view(request, pk):
	object = Post.objects.get(id=pk)
	context = {'object': object}
	return render(request, 'web/post/detail.html', context)


class PostUpdateView(LoginRequiredMixin, UpdateView):
	model = Post
	template_name = 'web/post/add.html'
	fields = ('text', 'media1')

	def get_success_url(self):
		return reverse('post', args=(self.object.id,))


class PostListView(LoginRequiredMixin, ListView):
	model = Post
	template_name = 'web/post/list.html'

