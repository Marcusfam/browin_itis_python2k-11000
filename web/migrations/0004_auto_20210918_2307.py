# Generated by Django 3.2.7 on 2021-09-18 19:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0003_auto_20210914_1943'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='birthdate',
            field=models.DateField(default='2021-12-25', max_length=128),
        ),
        migrations.AddField(
            model_name='user',
            name='name',
            field=models.CharField(default='Анонимный', max_length=128),
        ),
        migrations.AddField(
            model_name='user',
            name='surname',
            field=models.CharField(default='Анонимус', max_length=128),
        ),
    ]
