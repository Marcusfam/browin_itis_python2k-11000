# Generated by Django 3.2.7 on 2021-09-19 13:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0011_alter_post_media1'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='media1',
            field=models.ImageField(blank=True, null=True, upload_to='post_pictures/'),
        ),
    ]
