from django.forms import forms, fields, PasswordInput, ModelForm

from web.models import Post


class RegistrationForm(forms.Form):
    email = fields.EmailField(
        label='Email'
    )
    password = fields.CharField(
        label='Пароль',
        widget=PasswordInput()
    )
    password2 = fields.CharField(
        label='Повторите пароль',
        widget=PasswordInput()
    )
    name = fields.CharField(
        label='Ваше имя'
    )
    surname = fields.CharField(
        label='Ваша фамилия'
    )
    birthdate = fields.DateField(
        label='Дата рождения'
    )
    avatar = fields.ImageField(
        label='Загрузите аватар'
    )

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        if cleaned_data['password'] != cleaned_data['password2']:
            self.add_error('password', 'Пароли не совпадают')
        return cleaned_data


class LoginForm(forms.Form):
    email = fields.EmailField(
        label="Email"
    )
    password = fields.CharField(
        label='Пароль',
        widget=PasswordInput()
    )


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ('text', 'media1')
        labels = {'text':'Что у вас нового?',
                  'media1':'Покажите что-нибудь красивое'}
