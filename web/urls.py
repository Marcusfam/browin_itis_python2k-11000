"""testdjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from web.views import main_view, register_view, login_view, logout_view, post_add_view, post_detail_view, \
                      PostUpdateView, PostListView

urlpatterns = [
    path('', main_view, name='main'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', register_view, name='register'),
    path('post/add/', post_add_view, name='post-add'),
    path('post/<int:pk>/', post_detail_view, name='post'),
    path('post/<int:pk>/add/', PostUpdateView.as_view(), name='post-edit'),
    path('post/', PostListView.as_view(), name='posts'),
]