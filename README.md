# Browin_itis_python2k-11000

## Установка и запуск проекта

- `python3 -m venv .venv` - создать виртуальное окружение
- `cd .venv/bin/activate` - войти в виртуальное окружение
- `pip install -r requirements.txt` - установить зависимости
- `docker-compose up` - поднять базу данных PostgreSQL (если Вы не используете Docker, установите PostgreSQL
с официального сайта)
- `python manage.py migrate` - применить миграции к базе данных
- `python manage.py runserver` - запуск сервера для разработки 
